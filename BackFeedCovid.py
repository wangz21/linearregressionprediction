# -*- coding: UTF-8 -*- #
"""
@fileName:BackFeedCovid.py
@author:Dextor
@time:2024-01-03
"""
from sklearn.linear_model import RidgeCV
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

matplotlib.use('TkAgg')  # use TkAgg back-end

from DataProcessing import preprocess_bike_weather_data


def continuous_prediction(model, initial_data, start_date, end_date, features, weather_data):
    current_data = initial_data.copy()
    predictions = pd.Series(index=pd.date_range(start=start_date, end=end_date, freq='H'))

    for dt in predictions.index:
        row = pd.Series(index=features)
        # Process with the history data (basically will not get in)
        if dt in current_data.index:
            for feature in features:
                row[feature] = current_data.at[dt, feature]
        # Use the features to predict the data and will be used in the next prediction
        else:
            # Update the features
            row['hour'] = dt.hour
            row['day_of_week'] = dt.dayofweek
            for lag in range(1, n_lags + 1):
                lag_time = dt - pd.Timedelta(hours=lag)
                if lag_time in current_data.index:
                    value = current_data.loc[lag_time, 'USAGE_FREQUENCY']
                    if isinstance(value, pd.Series):
                        value = value.iloc[0]  # Insurance: If data have multiple instance
                    row[f'lag_{lag}'] = value
                elif lag_time in predictions.index:
                    row[f'lag_{lag}'] = predictions[lag_time]
                else:
                    row[f'lag_{lag}'] = np.nan

            for day in additional_days:
                day_time = dt - pd.Timedelta(days=day)
                if day_time in current_data.index:
                    value = current_data.loc[day_time, 'USAGE_FREQUENCY']
                    if isinstance(value, pd.Series):
                        value = value.iloc[0]   # Insurance
                    row[f'lag_{day * 24}_hour'] = value
                elif day_time in predictions.index:
                    value = predictions.loc[day_time]
                    if isinstance(value, pd.Series):
                        value = value.iloc[0]  # Insurance
                    row[f'lag_{day * 24}_hour'] = value
                else:
                    row[f'lag_{day * 24}_hour'] = np.nan

            for week in additional_weeks:
                week_time = dt - pd.Timedelta(weeks=week)
                if week_time in current_data.index:
                    value = current_data.loc[week_time, 'USAGE_FREQUENCY']
                    if isinstance(value, pd.Series):
                        value = value.iloc[0]
                    row[f'lag_{week * 7 * 24}_hour'] = value
                elif week_time in predictions.index:
                    value = predictions.loc[week_time]
                    if isinstance(value, pd.Series):
                        value = value.iloc[0]
                    row[f'lag_{week * 7 * 24}_hour'] = value
                else:
                    row[f'lag_{week * 7 * 24}_hour'] = np.nan

            # Use the hourly data do the update
            if dt in weather_data.index:
                for weather_feature in ['temp', 'tempmin', 'tempmax', 'preciptype']:
                    row[weather_feature] = weather_data.at[dt, weather_feature]
            row.ffill(inplace=True)  # Forward fill
            row.bfill(inplace=True)  # Backward fill if forward fill isn't applicable
            df_row = pd.DataFrame([row], columns=features)
            predicted_value = model.predict(df_row)[0]

            # Limit the predicted value between 0 and 100
            predicted_value = max(0, min(predicted_value, 100))

            # Store the limited value
            predictions[dt] = predicted_value
        new_row = pd.DataFrame(index=[dt], data=[predictions[dt]], columns=['USAGE_FREQUENCY'])
        new_row.ffill(inplace=True)  # Forward fill
        new_row.bfill(inplace=True)  # Backward fill if forward fill isn't applicable
        current_data = pd.concat([current_data, new_row])

    return predictions


def create_lagged_features(df, n_lags, additional_days=None, additional_weeks=None):
    """
    Create lagged features for a time series, ensuring correct time alignment.
    """

    # Function to create a lagged feature with correct time alignment
    def create_lagged_feature(df, lag_hours):
        lagged_series = []
        for idx, row in df.iterrows():
            lagged_time = idx - pd.Timedelta(hours=lag_hours)
            if lagged_time in df.index:
                lagged_value = df.at[lagged_time, 'USAGE_FREQUENCY']
            else:
                lagged_value = np.nan  # Missing data for this lagged time
            lagged_series.append(lagged_value)
        return pd.Series(lagged_series, index=df.index)

    # Create standard lags
    for lag in range(1, n_lags + 1):
        df[f'lag_{lag}'] = create_lagged_feature(df, lag)

    # Create additional day-based lags
    if additional_days:
        for day in additional_days:
            df[f'lag_{day * 24}_hour'] = create_lagged_feature(df, day * 24)

    # Create additional week-based lags
    if additional_weeks:
        for week in additional_weeks:
            df[f'lag_{week * 24 * 7}_hour'] = create_lagged_feature(df, week * 24 * 7)
    '''
    # If you don't want the missing value
    return df.dropna()
    '''
    # Impute missing values
    df.ffill(inplace=True)  # Forward fill
    df.bfill(inplace=True)  # Backward fill if forward fill isn't applicable
    return df


def plot_predicted_vs_actual(actual_sample, predicted_sample):
    plt.figure(figsize=(15, 5))

    # Set the format of x axis
    myFmt = mdates.DateFormatter('%m-%d %H:%M')

    # Plot the actual usage frequency
    plt.plot(actual_sample.index, actual_sample, label='Actual Usage Frequency', marker='o', linestyle='-')

    # Plot the prediction of usage frequency
    plt.plot(actual_sample.index, predicted_sample, label='Predicted Usage Frequency', marker='x', linestyle='-')

    # Set the label and figure
    plt.title(f'Comparison of Actual and Predicted Usage Frequency for {address}')
    plt.xlabel('Date and Time')
    plt.ylabel('Usage Frequency (%)')
    plt.legend()

    # Set the date format for x axis
    plt.gca().xaxis.set_major_formatter(myFmt)

    # Avoid overlapping
    plt.gcf().autofmt_xdate()

    plt.show()


# Training dataset, I need to add the dataset during the pandemic period to predict the effect of the pandemic
date_ranges = [
    ('2018-08-01', '2018-10-01'),
    ('2018-10-01', '2019-01-01'),
    ('2019-01-01', '2019-04-01'),
    ('2019-04-01', '2019-07-01'),
    ('2019-07-01', '2019-10-01'),
    ('2019-10-01', '2020-01-01'),
    ('2020-01-01', '2020-04-01'),
    ('2020-04-01', '2020-07-01'),
    ('2020-07-01', '2020-10-01'),
]
'''
    
'''

all_hourly_usage_rates = pd.DataFrame()

# address = 'Dame Street'
address = 'Charlemont Street'
# address = 'Parnell Street'
# address = 'Smithfield'

for start_date, end_date in date_ranges:
    # Because of in dublinbikes_20180701_20181001.csv, the dataset started from 20180801, so need to do some specify
    if start_date == '2018-08-01':
        file_bike_path = 'DublinBike Dataset/dublinbikes_20180701_20181001.csv'
    else:
        file_bike_path = 'DublinBike Dataset/dublinbikes_{}_{}.csv'.format(start_date.replace('-', '')[:8],
                                                                           end_date.replace('-', '')[:8])
    file_weather_path = 'DublinWeather Dataset/Dublin Weather 2018-08-01 to 2019-12-31.csv'

    target = 0
    if end_date == '2020-04-01':
        file_weather_path = 'DublinWeather Dataset/Dublin Weather 2020-01-01 to 2021-10-01.csv'
        target = 1
    if target == 1:
        file_weather_path = 'DublinWeather Dataset/Dublin Weather 2020-01-01 to 2021-10-01.csv'

    hourly_usage_rate, weather_data = preprocess_bike_weather_data(
        file_bike_paths=[file_bike_path],
        file_weather_paths=[file_weather_path],
        start_date=start_date,
        end_date=end_date,
        # Address='Dame Street'
        # Address='Charlemont Street'
        Address=address
        # Address='Smithfield'
    )

    hourly_usage_rate['hour'] = hourly_usage_rate.index.hour
    hourly_usage_rate['day_of_week'] = hourly_usage_rate.index.dayofweek

    all_hourly_usage_rates = pd.concat([all_hourly_usage_rates, hourly_usage_rate])

all_hourly_usage_rates.sort_index(inplace=True)

# Set the lag quality
n_lags = 3  # MSE: 466.8
additional_days = [1, 2, 3]  # Dame: 419.96, 11614, Charlemont: 325.49, 11643, Parnell: 296.26 11643
additional_weeks = [1, 2, 3]
df_lagged = create_lagged_features(all_hourly_usage_rates, n_lags, additional_days, additional_weeks)

# Extract the features and target value
features = [f'lag_{i}' for i in range(1, n_lags + 1)] + \
           [f'lag_{day * 24}_hour' for day in additional_days] + \
           [f'lag_{week * 7 * 24}_hour' for week in additional_weeks] + \
           ['hour', 'day_of_week', 'temp', 'tempmin', 'tempmax', 'preciptype']

# Extract the special timeline for training
covid_start_date = '2020-03-16'
start_test_date = '2019-10-01'
end_exclusion_date = '2020-03-16'  # Extract the data effected by the pandemic
mask_train = (df_lagged.index >= start_test_date) & (df_lagged.index <= covid_start_date)
mask_exclude = df_lagged.index >= end_exclusion_date
train_data = df_lagged.loc[mask_train & ~mask_exclude]

# Use the rest of the dataset as training data
week_data = df_lagged.loc[~mask_train & ~mask_exclude]

# Complete the training and testing data
X_train = train_data[features]
print(X_train)
y_train = train_data['USAGE_FREQUENCY']

# Initialize the Linear Regression Model with cross validation Ridge
alphas = np.logspace(-6, 6, 13)
ridge_cv = RidgeCV(alphas=alphas, store_cv_values=True)

# Train the model
ridge_cv.fit(X_train, y_train)

start_prediction_date = '2020-03-16 01:00'
end_prediction_date = '2020-09-30 00:00'
predicted_frequencies = continuous_prediction(
    model=ridge_cv,
    initial_data=train_data,
    start_date=start_prediction_date,
    end_date=end_prediction_date,
    features=features,
    weather_data=all_hourly_usage_rates  # Contain the Covid-19 Dataset
)

start_interested_prediction_date = '2020-09-10 00:00'
end_interested_prediction_date = '2020-09-29 23:00'
interested_predictions = predicted_frequencies[start_interested_prediction_date:end_interested_prediction_date]

# 从总的数据集中提取对应时间范围的实际数据
actual_data = all_hourly_usage_rates.loc[start_interested_prediction_date:end_interested_prediction_date, 'USAGE_FREQUENCY']

# 确保预测数据的时间范围与实际数据相匹配
interested_predictions = interested_predictions.reindex(actual_data.index)

# Plot the image
plot_predicted_vs_actual(actual_data, interested_predictions)

