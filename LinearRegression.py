# -*- coding: UTF-8 -*- #
"""
@fileName: LinearRegression.py
@author: Dextor
@time: 2024-01-01
"""
from sklearn.linear_model import RidgeCV
from sklearn.metrics import mean_squared_error
from sklearn.dummy import DummyRegressor
import numpy as np
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

matplotlib.use('TkAgg')  # use TkAgg back-end

from DataProcessing import preprocess_bike_weather_data


def create_lagged_features(df, n_lags, additional_days=None, additional_weeks=None):
    """
    Create lagged features for a time series, ensuring correct time alignment.
    """

    # Function to create a lagged feature with correct time alignment
    def create_lagged_feature(df, lag_hours):
        lagged_series = []
        for idx, row in df.iterrows():
            lagged_time = idx - pd.Timedelta(hours=lag_hours)
            if lagged_time in df.index:
                lagged_value = df.at[lagged_time, 'USAGE_FREQUENCY']
            else:
                lagged_value = np.nan  # Missing data for this lagged time
            lagged_series.append(lagged_value)
        return pd.Series(lagged_series, index=df.index)

    # Create standard lags
    for lag in range(1, n_lags + 1):
        df[f'lag_{lag}'] = create_lagged_feature(df, lag)

    # Create additional day-based lags
    if additional_days:
        for day in additional_days:
            df[f'lag_{day * 24}_hour'] = create_lagged_feature(df, day * 24)

    # Create additional week-based lags
    if additional_weeks:
        for week in additional_weeks:
            df[f'lag_{week * 24 * 7}_hour'] = create_lagged_feature(df, week * 24 * 7)
    '''
    # If you don't want the missing value
    return df.dropna()
    '''
    # Impute missing values
    df.ffill(inplace=True)  # Forward fill
    df.bfill(inplace=True)  # Backward fill if forward fill isn't applicable
    return df


def plot_predicted_vs_actual(actual_sample, predicted_sample):
    plt.figure(figsize=(15, 5))

    # Set the format of x axis
    myFmt = mdates.DateFormatter('%m-%d %H:%M')

    # Plot the actual usage frequency
    plt.plot(actual_sample.index, actual_sample, label='Actual Usage Frequency', marker='o', linestyle='-')

    # Plot the prediction of usage frequency
    plt.plot(actual_sample.index, predicted_sample, label='Predicted Usage Frequency', marker='x', linestyle='')

    # Set the label and figure
    plt.title(f'Comparison of Actual and Predicted Usage Frequency for {address}')
    plt.xlabel('Date and Time')
    plt.ylabel('Usage Frequency (%)')
    plt.legend()

    # Set the date format for x axis
    plt.gca().xaxis.set_major_formatter(myFmt)

    # Avoid overlapping
    plt.gcf().autofmt_xdate()

    plt.show()


# Training dataset, I need to add the dataset during the pandemic period to predict the effect of the pandemic
date_ranges = [
    ('2018-08-01', '2018-10-01'),
    ('2018-10-01', '2019-01-01'),
    ('2019-01-01', '2019-04-01'),
    ('2019-04-01', '2019-07-01'),
    ('2019-07-01', '2019-10-01'),
    ('2019-10-01', '2020-01-01'),
    ('2020-01-01', '2020-04-01')
]

all_hourly_usage_rates = pd.DataFrame()

address = 'Dame Street'
# address = 'Charlemont Street'
# address = 'Parnell Street'
# address = 'Smithfield'

for start_date, end_date in date_ranges:
    # Because of in dublinbikes_20180701_20181001.csv, the dataset started from 20180801, so need to do some specify
    if start_date == '2018-08-01':
        file_bike_path = 'DublinBike Dataset/dublinbikes_20180701_20181001.csv'
    else:
        file_bike_path = 'DublinBike Dataset/dublinbikes_{}_{}.csv'.format(start_date.replace('-', '')[:8],
                                                                           end_date.replace('-', '')[:8])
    file_weather_path = 'DublinWeather Dataset/Dublin Weather 2018-08-01 to 2019-12-31.csv'

    if end_date == '2020-04-01':
        file_weather_path = 'DublinWeather Dataset/Dublin Weather 2020-01-01 to 2021-10-01.csv'

    hourly_usage_rate, weather_data = preprocess_bike_weather_data(
        file_bike_paths=[file_bike_path],
        file_weather_paths=[file_weather_path],
        start_date=start_date,
        end_date=end_date,
        # Address='Dame Street'
        # Address='Charlemont Street'
        Address=address
        # Address='Smithfield'
    )

    hourly_usage_rate['hour'] = hourly_usage_rate.index.hour
    hourly_usage_rate['day_of_week'] = hourly_usage_rate.index.dayofweek

    all_hourly_usage_rates = pd.concat([all_hourly_usage_rates, hourly_usage_rate])

all_hourly_usage_rates.sort_index(inplace=True)

print(all_hourly_usage_rates)

# Set the lag quality
n_lags = 3  # MSE: 466.8
additional_days = [1, 2, 3]  # Dame: 419.96, 11614, Charlemont: 325.49, 11643, Parnell: 296.26 11643
additional_weeks = [1, 2, 3]
df_lagged = create_lagged_features(all_hourly_usage_rates, n_lags, additional_days, additional_weeks)

# Extract the features and target value
features = [f'lag_{i}' for i in range(1, n_lags + 1)] + \
           [f'lag_{day * 24}_hour' for day in additional_days] + \
           [f'lag_{week * 7 * 24}_hour' for week in additional_weeks] + \
           ['hour', 'day_of_week', 'temp', 'tempmin', 'tempmax', 'preciptype']

# Extract the special timeline for training
start_test_date = '2018-09-10'
end_test_date = '2018-09-17'
covid_start_date = '2020-03-16'

# Specify the date between these period and extract the date after the Covid start
mask = ((df_lagged.index >= start_test_date) &
        (df_lagged.index <= end_test_date) &
        (df_lagged.index < covid_start_date))

# Use mask to choose the week_data
week_data = df_lagged.loc[mask]

train_data = df_lagged.loc[~mask & (df_lagged.index < covid_start_date)]

# Complete the training and testing data
X_train = train_data[features]
y_train = train_data['USAGE_FREQUENCY']
X_test = week_data[features]
y_test = week_data['USAGE_FREQUENCY']

# Initialize the Linear Regression Model with cross validation Ridge
alphas = np.logspace(-6, 6, 13)
ridge_cv = RidgeCV(alphas=alphas, store_cv_values=True)

# Train the model
ridge_cv.fit(X_train, y_train)

# After fitting the model
feature_weights = ridge_cv.coef_
for feature, weight in zip(features, feature_weights):
    print(f"{feature}: {weight}")

# Print the best alpha
# After fitting the model, we can access the MSE values for each alpha
mse_values = ridge_cv.cv_values_.mean(axis=0)

# Now we can plot the MSE values against the alphas
plt.figure(figsize=(10, 6))
plt.semilogx(alphas, mse_values, marker='o', linestyle='-', color='blue')
plt.xlabel('Alpha')
plt.ylabel('Mean Squared Error')
plt.title('MSE for different alpha values on a logarithmic scale')
plt.show()

# Print the MSE for each alpha
for alpha, mse in zip(alphas, mse_values):
    print(f'Alpha: {alpha} - MSE: {mse}')
print(f'Best alpha: {ridge_cv.alpha_}')

# Use the best alpha to make predictions
y_pred = ridge_cv.predict(X_test)

# Limit the predicted values between 0 and 100
y_pred = np.clip(y_pred, 0, 100)

# Calculate the print the MSE
mse = mean_squared_error(y_test, y_pred)
print(f'Mean Squared Error: {mse}')

# Dummy Regression
dummy_regressor = DummyRegressor(strategy="mean")
dummy_regressor.fit(X_train, y_train)

y_dummy_pred = dummy_regressor.predict(X_test)
mse_dummy = mean_squared_error(y_test, y_dummy_pred)
print(f'Dummy Mean Squared Error: {mse_dummy}')

# Compare the performance
if mse < mse_dummy:
    print("The Ridge regression model performs better than the dummy model.")
else:
    print("The Ridge regression model does not perform better than the dummy model.")

if not week_data.empty:
    test_sample = week_data[features]
    actual_sample = week_data['USAGE_FREQUENCY']
    predicted_sample = ridge_cv.predict(test_sample)

    # Limit the predicted values between 0 and 100
    predicted_sample = np.clip(predicted_sample, 0, 100)

    plot_predicted_vs_actual(actual_sample, predicted_sample)
else:
    print("No continuous data found for the specified week.")

