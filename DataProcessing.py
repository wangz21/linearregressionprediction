# -*- coding: UTF-8 -*- #
"""
@fileName:DataProcessing.py
@author:Zicheng Wang
@time:2023-12-28
"""

import pandas as pd
import matplotlib
import matplotlib.pyplot as plt

matplotlib.use('TkAgg')  # use TkAgg back-end


# The step of data visualization
def data_visualize_usage_rate():
    file_bike_paths = ['DublinBike Dataset/dublinbikes_20200701_20201001.csv']
    file_weather_paths = ['DublinWeather Dataset/Dublin Weather 2020-07-01 to 2021-10-01.csv']

    # Load the dataset by using the file path
    df_bike = load_bike_dataset(file_bike_paths)
    df_weather = load_weather_dataset(file_weather_paths)
    df = merge_datasets(df_bike, df_weather)

    # Specify the address equal to 'Dame Street'
    filtered_df = df[df['ADDRESS'] == 'Dame Street'].copy()

    # Specify the particular date first
    start_date = '2020-07-06'
    end_date = '2020-09-06'
    mask = (filtered_df.index >= start_date) & (filtered_df.index < end_date)
    filtered_df = filtered_df.loc[mask]

    # Calculate the usage rate
    BIKE_STANDS = filtered_df['BIKE STANDS']
    AVAILABLE_BIKES = filtered_df['AVAILABLE BIKES']
    filtered_df['USAGE_RATE'] = ((BIKE_STANDS - AVAILABLE_BIKES) / BIKE_STANDS) * 100

    # Data visualization
    plt.figure(figsize=(16, 7))

    # Track whether the first instance of rain or no rain has been plotted for legend
    plotted_rain = False
    plotted_no_rain = False

    # Plot each point individually to set the color based on 'preciptype'
    for i, row in filtered_df.iterrows():
        if row['preciptype'] == 'rain' and not plotted_rain:
            plt.plot(row.name, row['USAGE_RATE'], marker='o', color='r', linestyle='', label='Rain')
            plotted_rain = True
        elif row['preciptype'] != 'rain' and not plotted_no_rain:
            plt.plot(row.name, row['USAGE_RATE'], marker='o', color='b', linestyle='', label='No Rain')
            plotted_no_rain = True
        else:
            color = 'r' if row['preciptype'] == 'rain' else 'b'
            plt.plot(row.name, row['USAGE_RATE'], marker='o', color=color, linestyle='')

    # Format x-axis labels to display weekdays and every day within one week
    x_labels = [date.strftime('%A') for date in filtered_df.index]  # Extract weekdays
    x_labels_unique = [x_labels[i] if i == 0 or x_labels[i] != x_labels[i - 1] else '' for i in range(len(x_labels))]
    plt.xticks(filtered_df.index, x_labels_unique, rotation=45)

    plt.title('Dame Street Bike Usage Rate')
    plt.xlabel('Day of the Week')
    plt.ylabel('Usage Rate (%)')

    # Add legend to the plot
    plt.legend()

    plt.tight_layout()
    plt.show()


# Load the bike dataset for the visualization
def load_bike_dataset(file_paths):
    df_list = []
    for file_path in file_paths:
        df = pd.read_csv(file_path, parse_dates=['TIME'], index_col='TIME')
        df_list.append(df)
    df_bike = pd.concat(df_list)
    return df_bike


# Load the weather dataset for the visualization
def load_weather_dataset(file_paths):
    df_list = []
    for file_path in file_paths:
        df = pd.read_csv(file_path, parse_dates=['datetime'], index_col='datetime')
        df_list.append(df)
    df_weather = pd.concat(df_list)
    return df_weather


# Merge data
def merge_datasets(df_bike, df_weather):
    # Change the column 'TIME' from index to simple
    df_bike = df_bike.reset_index()

    df_weather = df_weather.reset_index()
    # Set the index of weather to 'datetime' again
    df_weather.set_index('datetime', inplace=True)

    # Add a column with simple 'date'
    df_bike['date'] = df_bike['TIME'].dt.normalize()

    # Initializes the merged data set
    merged_data = pd.DataFrame()

    # Merge data for each address
    for address in df_bike['ADDRESS'].unique():
        address_data = df_bike[df_bike['ADDRESS'] == address]
        merged_address_data = pd.merge_asof(address_data.sort_values('TIME'),
                                            df_weather.sort_index(),
                                            left_on='date',
                                            right_index=True,
                                            direction='nearest')
        merged_data = pd.concat([merged_data, merged_address_data])

    # Reset the index column to 'TIME'
    merged_data['preciptype'].fillna('sun', inplace=True)
    merged_data.set_index('TIME', inplace=True)

    return merged_data


def preprocess_bike_weather_data(file_bike_paths, file_weather_paths, start_date, end_date, Address):
    """
    Load, merge, and filter bike and weather datasets.
    Calculate bike usage statistics within a specified date range.
    """
    # Load the dataset by using the file path
    df_bike = load_bike_dataset(file_bike_paths)
    df_weather = load_weather_dataset(file_weather_paths)
    df = merge_datasets(df_bike, df_weather)

    # Filter for 'Dame Street' and status 'Open'
    if Address:  # If address != null
        filtered_df = df[(df['ADDRESS'] == Address) & (df['STATUS'] == 'Open')]
    else:  # Address = null
        filtered_df = df[df['STATUS'] == 'Open']

    # Filter for specified date range
    mask = (filtered_df.index >= start_date) & (filtered_df.index < end_date)
    filtered_df = filtered_df.loc[mask]

    # Calculate the usage rate
    filtered_df['bike_changes'] = filtered_df['AVAILABLE BIKES'].diff().fillna(0)
    filtered_df['bikes_rented'] = filtered_df['bike_changes'].apply(lambda x: -x if x < 0 else 0)
    filtered_df['bikes_returned'] = filtered_df['bike_changes'].apply(lambda x: x if x > 0 else 0)
    filtered_df['USAGE_RATE'] = ((filtered_df['BIKE STANDS'] - filtered_df['AVAILABLE BIKES']) / filtered_df[
        'BIKE STANDS']) * 100

    # Use the apply function and the custom function for mapping
    def map_preciptype(value):
        if value == 'rain':
            return 1
        elif value == 'rain,snow':
            return 2
        else:
            return 0  # All other cases map to 0

    filtered_df['preciptype'] = filtered_df['preciptype'].apply(map_preciptype)

    # Resample hourly and calculate usage frequency
    hourly_usage_rate = filtered_df.resample('H').agg({
        'bikes_rented': 'sum',
        'bikes_returned': 'sum',
        'BIKE STANDS': 'mean',
        'preciptype': 'max',  # Assuming 'preciptype' is categorical
        'temp': 'max',       # Assuming temperature data doesn't need aggregation
        'tempmin': 'max',
        'tempmax': 'max',
        'USAGE_RATE': 'mean'
    })

    hourly_usage_rate['USAGE_FREQUENCY'] = (hourly_usage_rate['bikes_rented'] + hourly_usage_rate['bikes_returned']) / \
                                           hourly_usage_rate['BIKE STANDS'] * 100
    # hourly_usage_rate['USAGE_FREQUENCY'] = hourly_usage_rate['USAGE_RATE']
    hourly_usage_rate['USAGE_FREQUENCY'] = hourly_usage_rate['USAGE_FREQUENCY'].clip(upper=100)

    # hourly_usage_rate['hour'] = hourly_usage_rate.index.hour
    hourly_usage_rate['day_of_week'] = hourly_usage_rate.index.dayofweek

    hourly_usage_rate.dropna(inplace=True)

    return hourly_usage_rate, df_weather


def visualize_bike_usage(hourly_usage_rate, df_weather, start_date, end_date):
    """
    Visualize the bike usage frequency with respect to weather conditions.
    Plot usage frequency against time, and overlay temperature data.
    """
    plt.figure(figsize=(16, 7))

    # Plotting setup for rain, no rain, and snow
    plotted_rain = False
    plotted_no_rain = False
    plotted_snow = False
    handles, labels = [], []

    # Plot each point individually to set the color based on 'preciptype'
    for i, row in hourly_usage_rate.iterrows():
        color = 'b'  # Default color for no precipitation
        label = None
        if row['preciptype'] == 2:  # Check for snow
            color = 'k'  # Black for snow
            if not plotted_snow:
                label = 'Snow'
                plotted_snow = True
        elif row['preciptype'] == 1:  # Check for rain
            color = 'r'  # Red for rain
            if not plotted_rain:
                label = 'Rain'
                plotted_rain = True
        else:  # row['preciptype'] == 0 for no precipitation
            if not plotted_no_rain:
                label = 'No Rain'
                plotted_no_rain = True
        point, = plt.plot(i, row['USAGE_FREQUENCY'], marker='o', color=color, linestyle='', label=label)
        if label:
            handles.append(point)
            labels.append(label)

    # Formatting x-axis to show weekdays and days within a week
    x_labels = [date.strftime('%A') for date in hourly_usage_rate.index]
    x_labels_unique = [x_labels[i] if i == 0 or x_labels[i] != x_labels[i - 1] else '' for i in range(len(x_labels))]
    plt.xticks(hourly_usage_rate.index, x_labels_unique, rotation=45)

    plt.title('Dame Street Bike Usage Frequency')
    plt.xlabel('Day of the Week')
    plt.ylabel('Usage Frequency (%)')

    # Creating a second y-axis for temperature data
    ax2 = plt.gca().twinx()

    # Ensure the weather data is within the specified range
    print(df_weather.index.min())
    print(df_weather.index.max())

    df_weather_filtered = df_weather.loc[start_date:end_date]
    print(df_weather_filtered.head())

    # Get temperature data
    daily_temp = df_weather_filtered['temp']
    daily_temp_min = df_weather_filtered['tempmin']
    daily_temp_max = df_weather_filtered['tempmax']

    x_ticks = daily_temp.index

    # Plot temperature data with error bars for min/max values
    ax2.plot(x_ticks, daily_temp, 'o', color='lightblue', label='Daily Temperature')
    ax2.errorbar(x_ticks, daily_temp, yerr=[daily_temp - daily_temp_min, daily_temp_max - daily_temp], fmt='none', ecolor='lightgray', capsize=3)

    point, = ax2.plot(x_ticks, daily_temp, 'o', color='lightblue', label='Daily Temperature')
    handles.append(point)
    labels.append('Daily Temperature')

    plt.legend(handles, labels, loc='upper left')
    plt.tight_layout()
    plt.show()

    # Calculate and display descriptive statistics for usage frequency
    usage_stats = hourly_usage_rate['USAGE_FREQUENCY'].describe()
    print("Descriptive statistics for USAGE Frequency:")
    print(usage_stats)

    # Calculate specific statistics
    mean_usage_rate = hourly_usage_rate['USAGE_FREQUENCY'].mean()
    median_usage_rate = hourly_usage_rate['USAGE_FREQUENCY'].median()
    std_dev_usage_rate = hourly_usage_rate['USAGE_FREQUENCY'].std()
    min_usage_rate = hourly_usage_rate['USAGE_FREQUENCY'].min()
    max_usage_rate = hourly_usage_rate['USAGE_FREQUENCY'].max()

    # Print the specific statistics
    print(f"Mean Usage Frequency: {mean_usage_rate:.2f}%")
    print(f"Median Usage Frequency: {median_usage_rate:.2f}%")
    print(f"Standard Deviation of Usage Frequency: {std_dev_usage_rate:.2f}")
    print(f"Minimum Usage Frequency: {min_usage_rate:.2f}%")
    print(f"Maximum Usage Frequency: {max_usage_rate:.2f}%")

'''
file_bike_paths=['DublinBike Dataset/dublinbikes_20200101_20200401.csv'],
        file_weather_paths=['DublinWeather Dataset/Dublin Weather 2020-01-01 to 2021-10-01.csv'],
        start_date='2020-02-01',
        end_date='2020-04-01',
        Address='Dame Street'
'''

if __name__ == '__main__':
    # Use the data processing function
    hourly_usage_rate, df_weather = preprocess_bike_weather_data(
        Address='Dame Street',
        file_bike_paths=['DublinBike Dataset/dublinbikes_20211001_20220101.csv'],
        file_weather_paths=['DublinWeather Dataset/Dublin Weather 2021-10-22 to 2022-12-31.csv'],
        start_date='2021-10-22',
        end_date='2021-12-22'
    )
    '''
        file_bike_paths = ['DublinBike Dataset/dublinbikes_20190401_20190701.csv'],
        file_weather_paths = ['DublinWeather Dataset/Dublin Weather 2018-08-01 to 2019-12-31.csv'],

        file_bike_paths = ['DublinBike Dataset/dublinbikes_20200701_20201001.csv'],
        file_weather_paths = ['DublinWeather Dataset/Dublin Weather 2020-07-01 to 2021-10-01.csv'],

        file_bike_paths = ['DublinBike Dataset/dublinbikes_20210701_20211001.csv'],
        file_weather_paths = ['DublinWeather Dataset/Dublin Weather 2020-07-01 to 2021-10-01.csv'],

        file_bike_paths = ['DublinBike Dataset/dublinbikes_20211001_20220101.csv'],
        file_weather_paths = ['DublinWeather Dataset/Dublin Weather 2021-10-22 to 2022-12-31.csv'],

    '''

    '''
        start_date = '2019-04-01',
        end_date = '2019-06-01'

        start_date = '2020-07-06',
        end_date = '2020-09-06'

        start_date = '2021-07-01',
        end_date = '2021-09-01'

        start_date = '2021-10-22',
        end_date = '2021-12-22'
        '''
    # Show the data and image
    visualize_bike_usage(
        hourly_usage_rate=hourly_usage_rate,
        df_weather=df_weather,
        start_date='2021-10-22',
        end_date='2021-12-22'
    )

